<?php

use App\Http\Controllers\CinemaController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\SeanceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/cinema', [CinemaController::class, 'index']);
Route::get('/cinema/{uid}', [CinemaController::class, 'show']);
Route::post('/cinema', [CinemaController::class, 'store'])->middleware('jwt');
Route::patch('/cinema/{uid}', [CinemaController::class, 'update'])->middleware('jwt');
Route::delete('/cinema/{uid}', [CinemaController::class, 'destroy'])->middleware('jwt');

// Room routes
Route::get('/cinema/{cinemaUid}/rooms', [RoomController::class, 'index']);
Route::get('/cinema/{cinemaUid}/rooms/{uid}', [RoomController::class, 'show']);
Route::post('/cinema/{cinemaUid}/rooms', [RoomController::class, 'store'])->middleware('jwt');
Route::patch('/cinema/{cinemaUid}/rooms/{uid}', [RoomController::class, 'update'])->middleware('jwt');
Route::delete('/cinema/{cinemaUid}/rooms/{uid}', [RoomController::class, 'destroy'])->middleware('jwt');

// Séance routes
Route::get('/cinema/{cinemaUid}/rooms/{roomUid}/seances', [SeanceController::class, 'index']);
Route::post('/cinema/{cinemaUid}/rooms/{roomUid}/seances', [SeanceController::class, 'store'])->middleware('jwt');
Route::patch('/cinema/{cinemaUid}/rooms/{roomUid}/seances/{uid}', [SeanceController::class, 'update'])->middleware('jwt');
Route::delete('/cinema/{cinemaUid}/rooms/{roomUid}/seances/{uid}', [SeanceController::class, 'destroy'])->middleware('jwt');

Route::post('/movie/{movieUid}/reservations', [ReservationController::class, 'store']);
Route::post('reservations/{uid}/confirm', [ReservationController::class, 'confirm']);
Route::get('reservations/{uid}', [ReservationController::class, 'show'])->middleware('jwt');
Route::get('/movie/{movieUid}/reservations', [ReservationController::class, 'index'])->middleware('jwt');
Route::get('/reservations', [ReservationController::class, 'getConfirmedReservations']);


