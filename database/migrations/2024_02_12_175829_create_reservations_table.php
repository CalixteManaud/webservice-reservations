<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->uuid('uuid')->unique();
            $table->foreignUuid('room_uid')->references('uuid')->on('rooms')->cascadeOnDelete();
            $table->foreignUuid('seance_uid')->references('uuid')->on('seances')->cascadeOnDelete();
            $table->enum('status', ['open', 'expired', 'confirmed']);
            $table->integer('seats')->unsigned();
            $table->integer('rank')->unsigned();
            $table->timestamp('expires_at')->nullable();
            $table->uuid('movie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reservations');
    }
};
