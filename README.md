# WebService Reservations

## Contexte du projet


>Ce projet vise à créer une interface de programmation d'application (API) permettant aux utilisateurs de réserver des places de cinéma via des applications ou des sites web. Il implique la conception d'une architecture logicielle pour gérer les fonctionnalités telles que la recherche de films, la sélection des horaires et des sièges, le traitement des paiements et la gestion des réservations. Une attention particulière sera portée à la sécurité des données, à la performance de l'API et à sa documentation complète pour une utilisation efficace et sécurisée.

## 🛠 Prérequis

Avant de démarrer, assurez-vous d'avoir installé les outils nécessaires :
- [Docker](https://www.docker.com/) - Pour la conteneurisation de l'application.
- [Docker Compose](https://docs.docker.com/compose/) - Pour la gestion multi-conteneurs.

## 🔧 Installation et Lancement

### 🗃️ Clonage du Projet
Clonez le dépôt Git pour récupérer le projet :
```sh
https://gitlab.com/CalixteManaud/webservice-reservations
```

## 🔑 Génération de la clé

Pour la génération des clés, tapez la commande suivante avant de lancer Docker :

```sh
openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048
openssl rsa -pubout -in private_key.pem -out public_key.pem
```

Puis, il faut les convertir en base64

```sh
base64 private_key.pem
base64 public_key.pem
```
Et enfin les mettre dans le fichier .env

 ```.dotenv
PRIVATE_KEY=private_key.pem
PUBLIC_KEY=public_key.pem
```

### 🚜 Construction et Lancement des Conteneurs

Construisez et lancez les conteneurs Docker à l'aide de :

```sh
docker-compose build
docker-compose up -d
```
### 📶 Création d'un Réseau pour les Conteneurs

Créez un réseau Docker pour permettre la communication entre les conteneurs :

```sh
docker network create app-network
```

### 📝 Configuration Environnementale
Pour le lancement du projet, créez un fichier .env qui contiendra le contenu du fichier .env.example avec une légère modification dans les parties suivantes en mettant vos informations :

```makefile
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=le_nom_de_votre_db
DB_USERNAME=le_nom_de_votre_utilisateur
DB_PASSWORD=votre_mdp

EMAIL_USERNAME= # Adresse email de Google
EMAIL_PASSWORD= # Mot de passe d'une application Gmail
```
Pour générer le mot de passe, il faut aller sur votre profil Google (avec votre adresse mail), recherchez dans la barre de recherche "Mot de passe des applications". Une fois dessus, créez une application que vous pourriez nommer comme vous le souhaitez, puis générez un mot de passe en cliquant sur Créer.
Ce mot de passe là sera renseigné dans les champs EMAIL_PASSWORD de votre fichier .env

Si pas de dossier vendor, il faut installer les dépendances avec la commande suivante :

```sh
composer u
```

## 🌐 Accès au Projet

### Gestion de base de données


Pour gérer les migrations, tapez les commandes suivantes : 

```sh
docker-compose exec auth-app3 php artisan migrate
```

### Listening des emails avec Artisan


Pour surveiller et gérer les e-mails entrants de manière automatisée, utilisez la commande suivante :
```sh
php artisan email:listen
```

## 📦 Accès aux Outils

### Base de Données via PHPMyAdmin
Accédez à PHPMyAdmin via http://localhost:8098 avec les identifiants suivants :

```yaml
Identifiant : laravel
Mot de passe : laravel
```

### RabbitMQ

Pour accéder à RabbitMQ, rendez-vous sur http://localhost:15678 avec les identifiants suivants :

```yaml
Identifiant : laravel
Mot de passe : laravel
```

### API via Scramble

Vous pouvez tester les fonctionnalités de l'API avec Scramble. En allant sur le lien suivant : http://localhost:7020/docs/api
Vous pourrez tester les différentes routes de l'API.

### Fonctionnalités : POSTMAN

Vous pouvez tester les fonctionnalités de l'API avec POSTMAN. Pour explorer l'API, utilisez `http://localhost:7020/api`.
Vous pouvez importer le fichier `WebService Reservation.postman_collection.json` pour tester les différentes routes de l'API.

