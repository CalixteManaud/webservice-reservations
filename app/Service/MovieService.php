<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class MovieService
{
    protected Client $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => 'http://nginx-movie/',
        ]);
    }

    /**
     * @param string $movieUuid
     * @return bool
     */
    public function checkMovieExists(string $movieUuid): bool
    {
        try {
            $response = $this->httpClient->request('GET', '/api/movies/' . $movieUuid);

            if ($response->getStatusCode() === 200){
                $body = (string) $response->getBody();
                $data = json_decode($body, true);

                return isset($data['uid']) && $data['uid'] === $movieUuid;
            }
            return false;
        } catch (GuzzleException $e) {
            Log::error('Error checking movie exists: ' . $e->getMessage());
            return false;
        }
    }
}
