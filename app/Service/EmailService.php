<?php

namespace App\Service;

use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPMailer\PHPMailer\PHPMailer;

class EmailService
{
    private AMQPStreamConnection $connection;
    private AbstractChannel|AMQPChannel $channel;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('rabbitmq', 5672, env('RABBITMQ_DEFAULT_USER'), env('RABBITMQ_DEFAULT_PASS'));
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare('emailQueue', false, false, false, false);
    }

    public function listenForEmailsAndSend(): void
    {
        echo " [*] Waiting for emails. To exit press CTRL+C\n";

        $callback = function ($msg) {
            echo ' [x] Received ', $msg->body, "\n";
            $data = json_decode($msg->body, true);
            $this->sendEmail($data);
        };

        $this->channel->basic_consume('emailQueue', '', false, true, false, false, $callback);

        while ($this->channel->is_consuming())
            $this->channel->wait();

        $this->close();
    }

    public function sendEmail($data): void
    {
        $email = new PHPMailer(true);

        try {
            $email->isSMTP();
            $email->Host = 'smtp.gmail.com';
            $email->SMTPAuth = true;
            $email->Username = env('EMAIL_USERNAME');
            $email->Password = env('EMAIL_PASSWORD');
            //$email->SMTPDebug = 2;
            $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $email->Port = 587;

            $email->setFrom(env('EMAIL_USERNAME'), 'Mailer');
            foreach ($data['to'] as $recipient)
                $email->addAddress(trim($recipient));

            $email->isHTML(true);
            $email->Subject = $data['subject'];
            $email->Body = $data['body'];

            $email->send();
            echo " [x] Email sent\n";
        } catch (\Exception $e) {
            Log::error('error sending email: ' . $e->getMessage());
            echo " [x] Email not sent. Error: {$email->ErrorInfo}\n";
        }
    }
}
