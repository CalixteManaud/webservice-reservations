<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class AuthenticationService {
    protected Client $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => 'http://nginx-auth/'
        ]);
    }

    public function getEmail(): string
    {
        try {
            $response = $this->httpClient->request('GET', '/api/users');
            if ($response->getStatusCode() === 200) {
                $body = (string) $response->getBody();
                $data = json_decode($body, true);
                $emails = [];

                foreach($data as $user)
                    if (isset($user['email']))
                        $emails[] = $user['email'];

                return implode(', ', $emails);
            }
            return '';
        } catch (GuzzleException $e) {
            Log::error('error checking for email: ' . $e->getMessage());
            return '';
        }
    }

    public function getEmailSpecify($id): string
    {
        try{
            $response = $this->httpClient->request('GET', '/api/users/');
            if ($response->getStatusCode() === 200) {
                $body = (string) $response->getBody();
                $data = json_decode($body, true);
                $email = [];

                foreach($data as $user)
                    if (isset($user['uid']) && $user['uid'] == $id)
                        $email[] = $user['email'];

                return implode(', ', $email);
            }
            return '';
        } catch (GuzzleException $e) {
            Log::error('error checking for email: ' . $e->getMessage());
            return '';
        }
    }
}
