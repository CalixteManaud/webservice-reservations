<?php

namespace App\Service;
use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ReservationService {

    private AMQPStreamConnection $connection;
    private AbstractChannel|AMQPChannel $channel;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('rabbitmq', 5672, env('RABBITMQ_DEFAULT_USER'), env('RABBITMQ_DEFAULT_PASS'));
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare('emailQueue', false, false, false, false);
    }

    public function publishMessage($data): void
    {
        $messageBody = json_encode($data);
        $message = new AMQPMessage($messageBody, [
            'content_type' => 'application/json',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]);

        $this->channel->basic_publish($message, '', 'emailQueue');
        echo "Message published to emailQueue\n";
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
