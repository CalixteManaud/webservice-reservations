<?php

namespace App\Jobs;

use App\Models\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CheckReservationExpirations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $seanceUid;

    public function __construct($seanceUid)
    {
        $this->seanceUid = $seanceUid;
    }

    public function handle(): void
    {
        DB::transaction(function () {
            $expiredReservations = Reservation::where('seance_uid', $this->seanceUid)
                ->where('status', 'open')
                ->where('expires_at', '<', now())
                ->get();

            foreach ($expiredReservations as $expiredReservation) {
                $expiredReservation->status = 'expired';
                $expiredReservation->save();

                // Ajustez les rangs des autres réservations ouvertes
                $openReservations = Reservation::where('seance_uid', $this->seanceUid)
                    ->where('status', 'open')
                    ->where('rank', '>', $expiredReservation->rank)
                    ->orderBy('rank', 'asc')
                    ->get()
                    ->each(function ($nextReservation) {
                        $nextReservation->decrement('rank');
                    });

                foreach ($openReservations as $openReservation) {
                    $openReservation->decrement('rank');
                }
            }
        });
    }
}
