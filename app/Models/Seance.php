<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;
    protected $fillable =[
        'movie',
        'date',
        'room_uid',
        'cinema_uid'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_uid', 'uuid');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'seance_uid', 'uuid');
    }

    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'cinema_uid', 'uuid');
    }
}
