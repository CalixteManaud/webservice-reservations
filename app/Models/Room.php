<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'seats', 'cinema_uid'];
    public $timestamps = true;
    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'cinema_uid', 'uuid');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'room_uid', 'uuid');
    }

    public function seances()
    {
        return $this->hasMany(Seance::class, 'room_uid', 'uuid');
    }
}
