<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = ['room_uid', 'status', 'seats', 'rank', 'expires_at', 'seance_uid', 'movie'];

    public $timestamps = true;

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_uid', 'uuid');
    }

    public function seance()
    {
        return $this->belongsTo(Seance::class, 'seance_uid', 'uuid');
    }
}
