<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = true;

    public function rooms()
    {
        return $this->hasMany(Room::class, 'cinema_uid', 'uuid');
    }

    public function seances()
    {
        return $this->hasMany(Seance::class, 'cinema_uid', 'uuid');
    }
}
