<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class JWTVerify
{
    public function handle(Request $request, Closure $next)
    {
        try {
            $token = $request->bearerToken();
            if (!$token) {
                Log::info('No token provided');
                return response()->json(['error' => 'Token not provided'], 401);
            }

            $decoded = JWT::decode($token, new Key(base64_decode(env("PRIVATE_KEY")), 'HS512'));

            if (isset($decoded->data->userId) && $decoded->data->role === 'ROLE_ADMIN') {
                $user = $decoded->data->userId;
            } else {
                Log::info('UID not found in token');
                return response()->json(['error' => $decoded], 400);
            }

            $request->auth = $user;

            return $next($request);
        } catch (\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid token'], 401); // Pour les erreurs de token JWT
        } catch (\Exception $e) {
            Log::error('JWT error: ' . $e->getMessage());
            return response()->json(["message" => $e->getMessage()], 401);
        }
    }
}
