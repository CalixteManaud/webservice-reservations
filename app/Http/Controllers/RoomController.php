<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Room;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RoomController extends Controller
{
    /**
     * Permet de lister les salles de cinéma
     * @unauthenticated
     * @param Request $request
     * @param string $cinemaUid
     * @return JsonResponse
     */
    public function index(Request $request, string $cinemaUid): JsonResponse
    {
        try {
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $cinema = Cinema::where('uuid', $cinemaUid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 204);

            $rooms = $cinema->rooms()->get();

            if ($rooms->isEmpty())
                return response()->json(['message' => 'No rooms found'], 204);


            $roomData = $rooms->map(function ($room) {
                return [
                    'uid' => $room->uuid,
                    'name' => $room->name,
                    'seats' => $room->seats,
                    'created_at' => $room->created_at->toDateTimeString(),
                    'updated_at' => $room->updated_at->toDateTimeString(),
                ];
            });

            return response()->json($roomData, 200);
        } catch (\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid token'], 400);
        } catch (\Exception $e) {
            Log::error('Error fetching rooms: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet d'afficher une salle de cinéma
     * @unauthenticated
     * @param Request $request
     * @param string $cinemaUid
     * @param string $uid
     * @return JsonResponse
     */
    public function show(Request $request, string $cinemaUid, string $uid): JsonResponse
    {
        try {
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $cinema = Cinema::where('uuid', $cinemaUid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = $cinema->rooms()->where('uuid', $uid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 404);

            return response()->json([
                'uid' => $room->uuid,
                'name' => $room->name,
                'seats' => $room->seats,
                'created_at' => $room->created_at->toDateTimeString(),
                'updated_at' => $room->updated_at->toDateTimeString(),
            ], 200);
        } catch (\Exception $e) {
            Log::error('Error fetching room: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de créer une salle de cinéma
     * @param Request $request
     * @param string $cinemaUid
     * @return JsonResponse
     */
    public function store(Request $request, string $cinemaUid): JsonResponse
    {
        try {
            $request->bearerToken();
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:128',
                'seats' => 'required|integer|min:1'
            ]);

            if ($validator->fails())
                return response()->json($validator->errors(), 422);

            $cinema = Cinema::where('uuid', $cinemaUid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = new Room();
            $room->name = $request->name;
            $room->seats = $request->seats;
            $room->cinema_uid = $cinemaUid;
            $room->uuid = Str::uuid();
            $room->save();

            return response()->json([
                'uid' => $room->uuid,
                'name' => $room->name,
                'seats' => $room->seats,
                'created_at' => $room->created_at,
                'updated_at' => $room->updated_at,
            ], 201);
        } catch (\Exception $e) {
            Log::error('Error creating room: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de modifier une salle de cinéma
     * @param Request $request
     * @param string $cinemaUid
     * @param string $uid
     * @return JsonResponse
     */
    public function update(Request $request, string $cinemaUid, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $cinema = Cinema::where('uuid', $cinemaUid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 204);

            $room = $cinema->rooms()->where('uuid', $uid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 204);

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:128',
                'seats' => 'required|integer|min:1'
            ]);

            if ($validator->fails())
                return response()->json($validator->errors(), 422);

            $room->update($request->all());

            return response()->json([
                'uid' => $room->uuid,
                'name' => $room->name,
                'seats' => $room->seats,
                'created_at' => $room->created_at,
                'updated_at' => $room->updated_at,
            ], 200);
        } catch (\Exception $e) {
            Log::error('Error updating room: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de supprimer une salle de cinéma
     * @param Request $request
     * @param string $cinemaUid
     * @param string $uid
     * @return JsonResponse
     */
    public function destroy(Request $request, string $cinemaUid, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $cinema = Cinema::where('uuid', $cinemaUid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 204);

            $room = $cinema->rooms()->where('uuid', $uid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 204);

            $room->delete();

            return response()->json(['message' => 'Room deleted'], 200);
        } catch (\Exception $e) {
            Log::error('Error deleting room: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }
}
