<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\Routing\Attribute\Route;

class CinemaController extends Controller
{
    /**
     * Permet de lister les cinémas
     * @Route("/cinema", methods={"GET"})
     * @unauthenticated
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request) : JsonResponse
    {
        try{
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $cinemas = Cinema::all();

            if ($cinemas->isEmpty())
                return response()->json(['message' => 'No cinemas found'], 204);

            $cinemaData  = $cinemas->map(function ($cinema) {
                return [
                    'uid' => $cinema->uuid,
                    'name' => $cinema->name,
                    'created_at' => $cinema->created_at,
                    'updated_at' => $cinema->updated_at,
                ];
            });

            return response()->json($cinemaData, 200);
        } catch (\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid token'], 400);
        } catch (\Exception $e) {
            Log::error('Error fetching cinemas: ' . $e->getMessage());
            return response()->json(['message' => 'Error fetching cinemas'], 500);
        }
    }

    /**
     * Permet de récupérer un cinéma
     * @Route("/cinema/{uid}", methods={"GET"})
     * @unauthenticated
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function show(Request $request, string $uid) : JsonResponse
    {
        try {
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $cinema = Cinema::where('uuid', $uid)->first();

            if (!$cinema)
                return response()->json(['message' => 'Cinema not found'], 404);

            return response()->json([
                'uid' => $cinema->uuid,
                'name' => $cinema->name,
                'created_at' => $cinema->created_at,
                'updated_at' => $cinema->updated_at,
            ], 200);
        } catch (\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid token'], 400);
        } catch (\Exception $e) {
            Log::error('Error fetching cinema: '.$e->getMessage());
            return response()->json(['message' => 'Error fetching cinema'], 500);
        }
    }

    /**
     * Permet de créer un cinéma
     * @Route("/cinema", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $request->bearerToken();
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:128',
            ]);

            if ($validator->fails())
                return response()->json('The cinema object in the body is invalid', 422);

            $cinema = new Cinema();
            $cinema->uuid = Str::uuid();
            $cinema->name = $request->name;
            $cinema->save();

            return response()->json([
                'uid' => $cinema->uuid,
                'name' => $cinema->name,
                'created_at' => $cinema->created_at,
                'updated_at' => $cinema->updated_at,
            ], 201);
        } catch (\Exception $e) {
            Log::error('Error creating cinema: '.$e->getMessage());
            return response()->json(['message' => 'Error creating cinema'], 500);
        }
    }

    /**
     * Permet de mettre à jour un cinéma
     * @Route("/cinema/{uid}", methods={"PUT"})
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function update(Request $request, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $cinema = Cinema::where('uuid', $uid)->first();

            if (!$cinema)
                return response()->json(['message' => 'Cinema not found'], 204);

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:128'
            ]);

            if ($validator->fails())
                return response()->json('The cinema object in the body is invalid', 422);

            $cinema->name = $request->name;
            $cinema->save();

            return response()->json([
                'uid' => $cinema->uuid,
                'name' => $cinema->name,
                'created_at' => $cinema->created_at,
                'updated_at' => $cinema->updated_at,
            ], 200);
        } catch (\Exception $e) {
            Log::error('Error updating cinema: '.$e->getMessage());
            return response()->json(['message' => 'Error updating cinema'], 500);
        }
    }

    /**
     * Permet de supprimer un cinéma
     * @Route("/cinema/{uid}", methods={"DELETE"})
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function destroy(Request $request, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $cinema = Cinema::where('uuid', $uid)->first();

            if (!$cinema)
                return response()->json(['message' => 'Cinema not found'], 404);

            $cinema->delete();

            return response()->json(['message' => 'Cinema deleted'], 200);
        } catch (\Exception $e) {
            Log::error('Error deleting cinema: '.$e->getMessage());
            return response()->json(['message' => 'Error deleting cinema'], 500);
        }
    }
}
