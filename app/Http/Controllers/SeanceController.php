<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Room;
use App\Models\Seance;
use App\Service\AuthenticationService;
use App\Service\MovieService;
use App\Service\ReservationService;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SeanceController extends Controller
{
    /**
     * Permet de lister les séances d'une salle
     * @unauthenticated
     * @param Request $request
     * @param string $cinemaUuid
     * @param string $roomUid
     * @return JsonResponse
     */
    public function index(Request $request, string $cinemaUuid, string $roomUid): JsonResponse
    {
        try {
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $cinema = Cinema::where('uuid', $cinemaUuid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = Room::where('uuid', $roomUid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 404);

            $seances = $room->seances()->get();

            if ($seances->isEmpty()) {
                return response()->json(['message' => 'No seances found'], 404);
            }

            $seanceData = $seances->map(function ($seance) {
                return [
                    'uid' => $seance->uuid,
                    'movie' => $seance->movie,
                    'date' => $seance->date,
                    'created_at' => $seance->created_at->toDateTimeString(),
                    'updated_at' => $seance->updated_at->toDateTimeString(),
                ];
            });

            return response()->json($seanceData, 200);
        } catch (\Exception $e) {
            Log::error('Error fetching seances: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de créer une séance dans une salle
     * @param Request $request
     * @param string $cinemaUid
     * @param string $roomUid
     * @return JsonResponse
     */
    public function store(Request $request, string $cinemaUid, string $roomUid): JsonResponse
    {
        try {
            $request->bearerToken();
            $validated = $request->validate([
                'movie' => 'required|string',
                'date' => 'required|date',
            ]);

            $cinema = Cinema::where('uuid', $cinemaUid)->first();
            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = Room::where('uuid', $roomUid)->first();
            if (!$room)
                return response()->json(['error' => 'Room not found'], 404);

            $service = new MovieService();
            if (!$service->checkMovieExists($validated['movie']))
                return response()->json(['error' => 'Film not found'], 404);

            $seance = new Seance();
            $seance->uuid = Str::uuid();
            $seance->movie = $validated['movie'];
            $seance->date = $validated['date'];
            $seance->room = $roomUid;
            $seance->cinema = $cinemaUid;
            $seance->save();

            $authentication = new AuthenticationService();
            $emails = explode(',', $authentication->getEmail());

            $data = [
                'to' => $emails,
                'subject' => 'New Seance Created',
                'body' => "A new seance for the movie {$validated['movie']} has been created for date {$validated['date']}."
            ];

            $reservationService = new ReservationService();
            $reservationService->publishMessage($data);

            return response()->json([
                'uid' => $seance->uuid,
                'movie' => $seance->movie,
                'date' => $seance->date,
            ], 201);
        } catch (\Exception $e) {
            Log::error('Error creating seance: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de modifier séance dans une salle
     * @param Request $request
     * @param string $cinemaUid
     * @param string $roomUid
     * @param string $uid
     * @return JsonResponse
     */
    public function update(Request $request, string $cinemaUid, string $roomUid, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $seance = Seance::where('uuid', $uid)->first();

            if (!$seance)
                return response()->json(['error' => 'Seance not found'], 404);

            $cinema = Cinema::where('uuid', $cinemaUid)->first();
            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = Room::where('uuid', $roomUid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 404);

            $seance->room = $roomUid;
            $seance->cinema = $cinemaUid;

            $validated = $request->validate([
                'movie' => 'required|string',
                'date' => 'required|date',
            ]);

            $service = new MovieService();
            if (!$service->checkMovieExists($validated['movie'])) {
                return response()->json(['error' => 'Film not found'], 404);
            }

            $seance->update($validated);

            return response()->json([
                'uid' => $seance->uuid,
                'movie' => $seance->movie,
                'date' => $seance->date,
            ], 200);
        } catch (\Exception $e) {
            Log::error('Error updating seance: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de supprimer une séance dans une salle
     * @param Request $request
     * @param string $cinemaUuid
     * @param string $roomUuid
     * @param string $uid
     * @return JsonResponse
     */
    public function destroy(Request $request, string $cinemaUuid, string $roomUuid, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();
            $seance = Seance::where('uuid', $uid)->first();

            if (!$seance)
                return response()->json(['error' => 'Seance not found'], 404);

            $cinema = Cinema::where('uuid', $cinemaUuid)->first();

            if (!$cinema)
                return response()->json(['error' => 'Cinema not found'], 404);

            $room = Room::where('uuid', $roomUuid)->first();

            if (!$room)
                return response()->json(['error' => 'Room not found'], 404);

            $seance->delete();

            return response()->json(['message' => 'Seance deleted'], 200);
        } catch (\Exception $e) {
            Log::error('Error deleting seance: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }
}
