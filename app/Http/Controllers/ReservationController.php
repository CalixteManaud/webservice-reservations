<?php

namespace App\Http\Controllers;

use App\Jobs\CheckReservationExpirations;
use App\Models\Reservation;
use App\Models\Room;
use App\Models\Seance;
use App\Service\AuthenticationService;
use App\Service\MovieService;
use App\Service\ReservationService;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Attribute\Route;

class ReservationController extends Controller
{
    /**
     * Permet de rentrer dans le tunnel de réservation
     * @Route("/movies/{movieUid}/reservations", methods={"POST"})
     * @unauthenticated
     * @param Request $request
     * @param string $movieUid
     * @return JsonResponse
     */
    public function store(Request $request, string $movieUid) : JsonResponse
    {
        try {

            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $validated = $request->validate([
                'nbSeats' => 'required|integer|min:1',
                'room' => 'required|uuid|exists:rooms,uuid',
                'seance' => 'required|uuid|exists:seances,uuid'
            ]);

            $service = new MovieService();
            if (!$service->checkMovieExists($movieUid))
                return response()->json(['error' => 'Film not found'], 404);

            $lastRank = Reservation::where('seance_uid', $validated['seance'])->max('rank') ?? 0;

            $room  = Room::where('uuid', $validated['room'])->firstOrFail();
            $seance = Seance::where('uuid', $validated['seance'])->firstOrFail();

            $reservedSeatsCount = Reservation::where('seance_uid', $seance->uuid)
                ->where('status', '!=', 'expired')
                ->sum('seats');

            if ($validated['nbSeats'] > ($room->seats - $reservedSeatsCount))
                return response()->json(['error' => 'Not enough seats available'], 422);

            $reservation = new Reservation();
            $reservation->uuid = Str::uuid();
            $reservation->seats = $validated['nbSeats'];
            $reservation->room_uid = $validated['room'];
            $reservation->seance_uid = $validated['seance'];
            $reservation->status = 'open';
            $reservation->rank =  $lastRank + 1;
            $reservation->expires_at = now()->addMinutes(15);
            $reservation->movie = $movieUid;
            $reservation->save();

            return response()->json([
                'uid' => $reservation->uuid,
                'rank' => $reservation->rank,
                'status' => $reservation->status,
                'created_at' => $reservation->created_at->toDateTimeString(),
                'updated_at' => $reservation->updated_at->toDateTimeString(),
                'expires_at' => $reservation->expires_at->toDateTimeString()
            ], 201);

        } catch (ValidationException $e){
            return response()->json(['error' => 'Invalid reservation data'], 422);
        } catch (\UnexpectedValueException $e){
            return response()->json(['error' => 'Invalid token'], 401);
        } catch (\Exception $e) {
            Log::error('Error creating reservation: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de confirmer la réservation si le status le permet
     * @Route("/reservations/{uid}/confirm", methods={"POST"})
     * @unauthenticated
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function confirm(Request $request, string $uid): JsonResponse
    {
        try {
            $token = $request->bearerToken();
            $decode = JWT::decode($token, new Key(base64_decode(env('PRIVATE_KEY')), 'HS512'));

            if($decode->data->userId === null)
                return response()->json(['error' => 'no authorized'], 401);

            $reservation = Reservation::where('uuid', $uid)->first();

            if (!$reservation)
                return response()->json(['error' => 'Reservation not found'], 404);

            if ($reservation->status === 'expired' || $reservation->expires_at < now())
                return response()->json(['error' => 'Reservation expired'], 400);

            $reservation->status = 'confirmed';
            $reservation->save();

            DB::transaction(function () use ($reservation){
                $reservation->status = 'confirmed';
                $reservation->save();

                $openReservations = Reservation::where('seance_uid', $reservation->seance_uid)
                    ->where('status', 'open')
                    ->where('rank', '>', $reservation->rank)
                    ->orderBy('rank', 'asc')
                    ->get()
                    ->each(function ($nextReservation) {
                        $nextReservation->decrement('rank');
                    });

                foreach ($openReservations as $openReservation) {
                    $openReservation->decrement('rank');
                }
            });

            $AuthenticationService = new AuthenticationService();
            $reservationService = new ReservationService();
            $emailData = [
                'to' => [$AuthenticationService->getEmailSpecify($decode->data->userId)],
                'subject' => 'Confirmation of your reservation',
                'body' => "Your reservation for session {$reservation->seance_uid} is confirmed."
            ];

            $reservationService->publishMessage($emailData);

            return response()->json('Réservation effectuée avec succès', 201);
        } catch (ValidationException $e) {
            return response()->json(['error' => 'Invalid reservation data'], 422); // Pour les erreurs de validation
        } catch (\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid token'], 401); // Pour les erreurs de token JWT
        } catch (\Exception $e) {
            Log::error('Error confirming reservation: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500); // Pour toutes les autres erreurs
        }
    }

    /**
     * Liste toutes les réservations en cours pour un film
     * @Route("/movies/{movieUuid}/reservations", methods={"GET"})
     * @param Request $request
     * @param string $movieUuid
     * @return JsonResponse
     */
    public function index(Request $request, string $movieUuid): JsonResponse
    {
        try {
            $request->bearerToken();
            $service = new MovieService();
            if (!$service->checkMovieExists($movieUuid)) {
                return response()->json(['error' => 'Film not found'], 404);
            }

            $reservations = Reservation::all();

            return response()->json($reservations, 200);
        } catch (\Exception $e) {
            Log::error('Error fetching reservations: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de récupérer le détail d'une réservation
     * @Route("/reservations/{uid}", methods={"GET"})
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function show(Request $request, string $uid): JsonResponse
    {
        try {
            $request->bearerToken();

            $reservation = Reservation::where('uuid', $uid)
                ->first(['uuid', 'rank', 'status', 'created_at', 'updated_at', 'expires_at']);
            if (!$reservation) {
                return response()->json(['error' => 'Reservation not found'], 404);
            }

            return response()->json($reservation, 200);
        } catch (\Exception $e) {
            Log::error('Error fetching reservation: '.$e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    /**
     * Permet de lister les réservations confirmées pour l'api film
     * @return JsonResponse
     */
    public function getConfirmedReservations(): JsonResponse
    {
        $confirmedReservations = Reservation::where('status', 'confirmed')->get();
        return response()->json($confirmedReservations, 200);
    }
}
