<?php

namespace App\Console\Commands;

use App\Jobs\CheckReservationExpirations;
use App\Models\Reservation;
use Illuminate\Console\Command;

class ExpireReservation extends Command
{
    protected $signature = 'reservations:expire';
    protected $description = 'Expire reservations that have passed their expiration date.';

    public function handle(): void
    {
        $seancesUIds = Reservation::select('seance_uid')
            ->where('status', '=', 'open')
            ->where('expires_at', '<', now())
            ->groupBy('seance_uid')
            ->get()
            ->pluck('seance_uid');

        foreach ($seancesUIds as $seanceUid) {
            CheckReservationExpirations::dispatch($seanceUid);
        }
        $this->info('Expired reservations check dispatched for ' . $seancesUIds->count() . ' seance(s).');
    }
}
