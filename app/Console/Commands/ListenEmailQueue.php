<?php

namespace App\Console\Commands;

use App\Service\EmailService;
use Illuminate\Console\Command;

class ListenEmailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen to email queue and send emails';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->info('Listening to email queue...');
        $emailService = new EmailService();
        $emailService->listenForEmailsAndSend();

        $this->info('Listening to email queue finished');
    }
}
